package aleksy.pwr.javaa.knapsackthreads.logic.command;

import aleksy.abspec.commandaplicationengine.annotation.Command;
import aleksy.abspec.commandaplicationengine.model.AbstractCommand;
import aleksy.pwr.javaa.knapsacklib.logic.api.Algorithm;
import aleksy.pwr.javaa.knapsackthreads.constants.InstanceConfigConstants;
import aleksy.pwr.javaa.knapsackthreads.constants.RunConstants;
import aleksy.pwr.javaa.knapsackthreads.enumerate.CacheType;
import aleksy.pwr.javaa.knapsackthreads.logic.KnapsackThread;
import aleksy.pwr.javaa.knapsackthreads.logic.memory.Cache;
import aleksy.pwr.javaa.knapsackthreads.mbean.SystemConfig;
import aleksy.pwr.javaa.knapsackthreads.model.InstanceConfig;
import aleksy.pwr.javaa.knapsackthreads.util.Logger;
import aleksy.pwr.javaa.knapsackthreads.util.RunCommandUtil;

import javax.management.*;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

@Command(name = RunCommand.NAME, description = "starts the application")
public class RunCommand extends AbstractCommand {
   /**
    * Command name
    */
   static final String NAME = "run";

   private final String ARG_THREADS = "thr";
   private final String ARG_STACK = "st";
   private final String ARG_SEED = "sd";
   private final String ARG_LIFETIME = "lf";
   private final String ARG_CAPACITY = "cap";
   private final String ARG_NUMBER_OF_ITEMS = "it";
   private final String ARG_WEIGHT = "wg";
   private final String FLAG_STRONG_REFERENCE = "strong";
   private final String ARG_MEM_LIM = "ml";

   private List<Thread> threads;
   private int threadsCount;
   private int numberOfThreads;
   private int boundOfSeeds;
   private int lifetime;
   private int maxCapacity;
   private int maxNumberOfItems;
   private int maxItemWeight;
   private InstanceConfig instanceConfig;
   private Cache cache;
   private List<Algorithm> algorithms;

   @Override
   protected String getInfo() {
      return "Starts the Knapsack Threads application.";
   }

   @Override
   protected void run() {
      cache = createCache();
      numberOfThreads = (int) chooseFirstIfNotNull(getArguments().get(ARG_THREADS),
         RunConstants.DEFAULT_NUMBER_OF_THREADS);
      boundOfSeeds = (int) chooseFirstIfNotNull(getArguments().get(ARG_SEED),
         RunConstants.DEFAULT_BOUND_OF_SEEDS);
      lifetime = (int) chooseFirstIfNotNull(getArguments().get(ARG_LIFETIME),
         RunConstants.DEFAULT_LIFETIME_IF_THREAD);
      maxCapacity = (int) chooseFirstIfNotNull(getArguments().get(ARG_CAPACITY),
         InstanceConfigConstants.DEFAULT_MAX_CAPACITY);
      maxNumberOfItems = (int) chooseFirstIfNotNull(getArguments().get(ARG_NUMBER_OF_ITEMS),
         InstanceConfigConstants.DEFAULT_MAX_NUMBER_OF_ITEMS);
      maxItemWeight = (int) chooseFirstIfNotNull(getArguments().get(ARG_WEIGHT),
         InstanceConfigConstants.DEFAULT_MAX_ITEM_WEIGHT);

      Logger.log("STARTING THE APPLICATION.");
      Logger.log("Running parameters:");
      Logger.log("    Number of threads: " + numberOfThreads);
      Logger.log("    Cache size: " + boundOfSeeds);
      Logger.log("    Cache type: " + cache.getCacheType());
      Logger.log("    Default thread lifetime: " + lifetime);
      Logger.log("Knapsack problem instance parameters:");
      Logger.log("    Maximal capacity of knapsack: " + maxCapacity);
      Logger.log("    Maximal number of items: " + maxNumberOfItems);
      Logger.log("    Maximal weight of item: " + maxItemWeight);
      instanceConfig = InstanceConfig.create(maxCapacity, maxNumberOfItems, maxItemWeight);
      threads = new ArrayList<>();
      algorithms = new Vector<>();
      for (threadsCount = 0; threadsCount < numberOfThreads; threadsCount++) {
         threads.add(new KnapsackThread("THREAD_" + threadsCount, boundOfSeeds, lifetime, instanceConfig, cache, algorithms));
      }
      try {
         createMBean();
      } catch (MalformedObjectNameException | NotCompliantMBeanException | MBeanRegistrationException | InstanceAlreadyExistsException e) {
         e.printStackTrace();
      }
      runThreads(threads);
      Random random = new Random();
      while (atLeastOneThreadIsAlive(threads)) {
         try {
            Thread.sleep(1000 + random.nextInt(2000));
            RunCommandUtil.loadAlgorithms(algorithms);
         } catch (InterruptedException | MalformedURLException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
         }
         String waiting = "";
         int livingThreads = getNumberOfLivingThreads(threads);
         if (livingThreads != 0)
            waiting = "Waiting...";
         Logger.log(livingThreads + " thread(s) are alive. " + waiting);
         if (livingThreads == 0)
            Logger.log("Each thread ends its lifecycle.");
         logCacheStatus(cache);
      }
   }

   private void createMBean() throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException {
      MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
      SystemConfig mBean = new SystemConfig(threads, this);
      ObjectName name = new ObjectName("aleksy.pwr.javaa.knapsackthreads.mbean:type=SystemConfig");
      mbs.registerMBean(mBean, name);
   }

   private Cache createCache() {
      if (getFlags().contains(FLAG_STRONG_REFERENCE) || getFlags().isEmpty())
         return new Cache(CacheType.STRONG_REFERENCE, getArguments().get(ARG_MEM_LIM) == null ? RunConstants.DEFAULT_MEMORY_LIMIT : Integer.parseInt(getArguments().get(ARG_MEM_LIM)));
      return null;
   }

   @Override
   public void init() {
      addAvailableArgument(ARG_THREADS, "number of threads (default: " + RunConstants.DEFAULT_NUMBER_OF_THREADS + ")");
      addAvailableArgument(ARG_STACK, "stack size (unused)");
      addAvailableArgument(ARG_SEED, "size of cache (default: " + RunConstants.DEFAULT_BOUND_OF_SEEDS + ")");
      addAvailableArgument(ARG_MEM_LIM, "memory limit for the cache (default: " + RunConstants.DEFAULT_MEMORY_LIMIT + ")");
      addAvailableArgument(ARG_LIFETIME, "lifetime of each thread (default: " + RunConstants.DEFAULT_LIFETIME_IF_THREAD + ")");
      addAvailableArgument(ARG_CAPACITY, "maximal capacity of knapsack (default: " + InstanceConfigConstants.DEFAULT_MAX_CAPACITY + ")");
      addAvailableArgument(ARG_NUMBER_OF_ITEMS, "maximal number of items per instance of problem (default: " + InstanceConfigConstants.DEFAULT_MAX_NUMBER_OF_ITEMS + ")");
      addAvailableArgument(ARG_WEIGHT, "maximal weight of item (default: " + InstanceConfigConstants.DEFAULT_MAX_ITEM_WEIGHT + ")");
      addAvailableFlag(FLAG_STRONG_REFERENCE, "sets cache type to hash map");
      addExampleOfUsage().description("starts the application with default number of threads");
      addExampleOfUsage().argument(ARG_THREADS, "5")
         .description("starts the application with 5 threads");
      addExampleOfUsage()
         .argument(ARG_THREADS, "10")
         .argument(ARG_LIFETIME, "200")
         .argument(ARG_SEED, "1000")
         .argument(ARG_CAPACITY, "30")
         .argument(ARG_NUMBER_OF_ITEMS, "20")
         .argument(ARG_WEIGHT, "10")
         .description("starts the application with given arguments");
   }

   private void logCacheStatus(Cache cache) {
      int cacheCalls = cache.getCacheCalls();
      int cacheMisses = cache.getCacheMisses();
      int cacheHits = cacheCalls - cacheMisses;
      double percentageMisses = (double) cacheMisses / (double) cacheCalls;
      double percentageHits = (double) cacheHits / (double) cacheCalls;
      Logger.log("==================================");
      Logger.log("Cache status:");
      Logger.log("Cache type:     " + cache.getCacheType());
      Logger.log("Calls to cache: " + cacheCalls);
      Logger.log("Hits:           " + cacheHits + "   [" + roundPercentage(percentageHits) + "%]");
      Logger.log("Misses:         " + cacheMisses + "   [" + roundPercentage(percentageMisses) + "%]");
      Logger.log("Cache size:     " + cache.size());
      Logger.log("==================================");
   }

   public void addAndStartNewThread() {
      KnapsackThread knapsackThread = new KnapsackThread("THREAD_" + threadsCount++, boundOfSeeds, lifetime, instanceConfig, cache, algorithms);
      threads.add(knapsackThread);
      knapsackThread.start();
   }


   public void stopAndDeleteThread() {
      Thread thread = threads.get(new Random().nextInt(threads.size()));
      thread.interrupt();
      threads.remove(thread);
   }

   private Object chooseFirstIfNotNull(Object first, Object second) {
      if (first != null)
         return Integer.parseInt((String) first);
      return second;
   }

   private double roundPercentage(double x) {
      x *= 10000;
      x = Math.round(x);
      x /= 100;
      return x;
   }

   private int getNumberOfLivingThreads(List<Thread> threads) {
      int living = 0;
      for (Thread thread : threads) {
         if (thread.isAlive())
            living++;
      }
      return living;
   }

   private boolean atLeastOneThreadIsAlive(List<Thread> threads) {
      for (Thread thread : threads) {
         if (thread.isAlive())
            return true;
      }
      return false;
   }

   private void runThreads(List<Thread> threads) {
      for (Thread t : threads) {
         t.start();
      }
   }

   @Override
   protected boolean handleErrors() {
      if (getFlags().size() > 1) {
         Logger.log("In this command flag can be only one.");
         return true;
      }
      return super.handleErrors();
   }
}
