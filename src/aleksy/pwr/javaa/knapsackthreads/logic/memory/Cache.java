package aleksy.pwr.javaa.knapsackthreads.logic.memory;

import aleksy.pwr.javaa.knapsacklib.common.model.Knapsack;
import aleksy.pwr.javaa.knapsacklib.util.Logger;
import aleksy.pwr.javaa.knapsackthreads.enumerate.CacheCallStatus;
import aleksy.pwr.javaa.knapsackthreads.enumerate.CacheType;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.*;

/**
 * Cache memory class
 */
public class Cache {
   private Map<Long, Knapsack> cache;
   private int cacheCalls;
   private int cacheMisses;
   private CacheType cacheType;
   private int memoryLimit;

   /**
    * The constructor
    * @param type of cache
    * @param i
    */
   public Cache(CacheType type, int memoryLimit) {
      if(type == CacheType.STRONG_REFERENCE)
         cache = new HashMap<>();
      cacheCalls = 0;
      cacheMisses = 0;
      this.cacheType = type;
      this.memoryLimit = memoryLimit;
   }

   /**
    * Getter for counter for cache calls
    *
    * @return cache calls
    */
   public synchronized int getCacheCalls() {
      return cacheCalls;
   }

   /**
    * Getter for counter of cache misses
    *
    * @return cache misses
    */
   public synchronized int getCacheMisses() {
      return cacheMisses;
   }

   /**
    * Getter for cache type
    * @return cache type
    */
   public synchronized CacheType getCacheType() {
      return cacheType;
   }

   /**
    * Adds new solution with its seed to the cache
    *
    * @param seed     of the solution
    * @param knapsack solution of a problem
    */
   public synchronized CacheCallStatus add(long seed, Knapsack knapsack) {
      if (cache.size() == memoryLimit) {
         List<Long> cacheList = new ArrayList<>();
         cache.forEach((k, v) -> cacheList.add(k));
         Long l = cacheList.get(new Random().nextInt(cacheList.size()));
         cache.remove(l);
         Logger.log("CACHE", "Memory limit. Removing an element.");
      }
      Object result = cache.putIfAbsent(seed, knapsack);
      if (result == null) {
         incrementCacheMisses();
         return CacheCallStatus.MISS;
      } else {
         return CacheCallStatus.HIT;
      }
   }

   /**
    * Checks if seed in the cache exists as a key
    *
    * @param seed key in the cache
    * @return information if key exists
    */
   public synchronized boolean checkIfSeedExists(long seed) {
      incrementCacheCalls();
      Object knapsack = cache.get(seed);
      return knapsack != null;
   }

   /**
    * Clears the cache
    */
   public synchronized void clear() {
      cache.clear();
      cacheMisses = 0;
      cacheCalls = 0;
   }

   public synchronized int size() {
      return cache.size();
   }

   private synchronized void incrementCacheCalls() {
      cacheCalls++;
   }

   private synchronized void incrementCacheMisses() {
      cacheMisses++;
   }


}

