package aleksy.pwr.javaa.knapsackthreads.logic;

import aleksy.pwr.javaa.knapsacklib.common.model.ResultKnapsack;
import aleksy.pwr.javaa.knapsacklib.configuration.KnapsackConfig;
import aleksy.pwr.javaa.knapsacklib.configuration.impl.RandomSearchConfig;
import aleksy.pwr.javaa.knapsacklib.logic.api.Algorithm;
import aleksy.pwr.javaa.knapsacklib.logic.impl.RandomSearchAlgorithm;
import aleksy.pwr.javaa.knapsackthreads.enumerate.CacheCallStatus;
import aleksy.pwr.javaa.knapsackthreads.logic.memory.Cache;
import aleksy.pwr.javaa.knapsackthreads.model.InstanceConfig;
import aleksy.pwr.javaa.knapsackthreads.model.KnapsackProblemInstance;
import aleksy.pwr.javaa.knapsackthreads.util.KnapsackProblemInstances;
import aleksy.pwr.javaa.knapsackthreads.util.Logger;

import java.util.List;
import java.util.Random;

/**
 * Thread of Knapsack Threads Application
 */
public class KnapsackThread extends Thread {
   private String name;
   private long bound;
   private int lifetime;
   private Cache cache;
   private InstanceConfig instanceConfig;
   private List<Algorithm> algorithms;
   private Random random;

   /**
    * The constructor
    *
    * @param name           of thread
    * @param bound          of seeds
    * @param lifetime       of thread
    * @param instanceConfig configuration object of problem instance
    * @param cache          instance
    * @param algorithms     list
    */
   public KnapsackThread(String name, long bound, int lifetime, InstanceConfig instanceConfig, Cache cache, List<Algorithm> algorithms) {
      this.name = name;
      this.bound = bound;
      this.lifetime = lifetime;
      this.instanceConfig = instanceConfig;
      this.cache = cache;
      this.algorithms = algorithms;
      this.random = new Random();
   }

   @Override
   public void run() {
      while (lifetime-- > 0) {
         try {
            Thread.sleep(400 + random.nextInt(1200));
         } catch (InterruptedException e) {
            lifetime = 0;
         }
         String byeMessage = "";
         if (lifetime == 0)
            byeMessage = " Finishing, bye!";
         KnapsackProblemInstance instance = KnapsackProblemInstances.generate(random.nextInt((int) bound), instanceConfig);
         Logger.log(name, "Checking if seed " + instance.getSeed() + " already exists in the cache.");
         Algorithm algorithm = chooseAlgorithm();
         if (algorithm == null) {
            Logger.log(name, "Algorithms not found! Lifetime: " + lifetime + byeMessage);
         } else if (!cache.checkIfSeedExists(instance.getSeed())) {
            KnapsackConfig config = generateConfig(algorithm);
            Logger.log(name, "Seed " + instance.getSeed() + " doesn't exists in the cache. " +
               "Chosen algorithm: " + algorithm.getDescription().getTitle());
            ResultKnapsack knapsack = algorithm.process(instance.getItems(), instance.getEmptyKnapsack(), config);
            CacheCallStatus status = cache.add(instance.getSeed(), knapsack);
            Logger.log(name, "Solved in " + knapsack.getTimeOfProcessing().getNano() / 1000d
               + " ms, Status: " + status + ", lifetime: " + lifetime + byeMessage);
         } else {
            Logger.log(name, "Seed " + instance.getSeed() + " already exists in the cache. Status: "
               + CacheCallStatus.HIT + ", lifetime: " + lifetime + byeMessage);
         }
      }

   }

   private Algorithm chooseAlgorithm() {
      if (algorithms.isEmpty())
         return null;
      int index = random.nextInt(algorithms.size());
      return algorithms.get(index);
   }

   private KnapsackConfig generateConfig(Algorithm algorithm) {
      if (algorithm instanceof RandomSearchAlgorithm)
         return new RandomSearchConfig(100);
      return null;
   }
}
