package aleksy.pwr.javaa.knapsackthreads.runner;

import aleksy.abspec.commandaplicationengine.exception.CaeException;
import aleksy.abspec.commandaplicationengine.runner.CaeRunner;
import aleksy.abspec.commandaplicationengine.runner.CaeRunnerBuilder;
import aleksy.pwr.javaa.knapsackthreads.constants.Application;

/**
 * Application main runner
 */
public class KnapsackThreadsRunner {
   /**
    * Runs the application
    *
    * @param args list
    * @throws IllegalAccessException by CAE
    * @throws CaeException           by CAE
    * @throws InstantiationException by CAE
    */
   public void run(String[] args) throws IllegalAccessException, InstantiationException, CaeException {
      new CaeRunnerBuilder().createForApplication(Application.NAME,
         Application.VERSION, Application.AUTHOR)
         .withDefaultPrefixes()
         .withCommandName(Application.COMMAND_NAME)
         .withInformation(Application.INFORMATION)
         .withPackage(Application.PACKAGE)
         .fill(new CaeRunner() {
            @Override
            protected void globalRun(String[] strings) {
            }
         }).run(args);
   }
}
