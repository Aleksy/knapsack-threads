package aleksy.pwr.javaa.knapsackthreads;

import aleksy.abspec.commandaplicationengine.exception.CaeException;
import aleksy.pwr.javaa.knapsackthreads.runner.KnapsackThreadsRunner;

/**
 * Main class
 */
public class KnapsackThreadsApp {

   /**
    * Main method
    *
    * @param args list of arguments
    * @throws IllegalAccessException reflection
    * @throws CaeException           by CAE
    * @throws InstantiationException reflection
    */
   public static void main(String[] args) throws Exception {
      new KnapsackThreadsRunner().run(args);
   }
}
