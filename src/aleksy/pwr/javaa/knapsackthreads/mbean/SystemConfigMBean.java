package aleksy.pwr.javaa.knapsackthreads.mbean;

public interface SystemConfigMBean {

   void setThreadCount(int noOfThreads);

   int getThreadCount();
}
