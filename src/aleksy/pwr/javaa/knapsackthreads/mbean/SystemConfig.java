package aleksy.pwr.javaa.knapsackthreads.mbean;

import aleksy.pwr.javaa.knapsackthreads.logic.command.RunCommand;

import java.util.List;

public class SystemConfig implements SystemConfigMBean {
   private List<Thread> threads;
   private RunCommand runCommand;
   private int threadCount;

   public SystemConfig(List<Thread> threads, RunCommand runCommand) {
      this.threads = threads;
      this.runCommand = runCommand;
      this.threadCount = threads.size();
   }

   @Override
   public void setThreadCount(int threadCount) {
      if (vaildate(threadCount)) {
         if (threadCount == threads.size()) {
            return;
         }
         if (threadCount > threads.size()) {
            int i = threadCount;
            while (i-- > this.threadCount) {
               runCommand.addAndStartNewThread();
            }
         }
         if (threadCount < threads.size()) {
            int i = threadCount;
            while (i++ < this.threadCount) {
               runCommand.stopAndDeleteThread();
            }
         }
      }
      this.threadCount = threadCount;
   }

   private boolean vaildate(int threadCount) {
      return threadCount >= 0;
   }


   @Override
   public int getThreadCount() {
      return threadCount;
   }

}