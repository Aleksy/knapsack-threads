package aleksy.pwr.javaa.knapsackthreads.constants;

import aleksy.pwr.javaa.knapsackthreads.logic.command.RunCommand;

/**
 * Constants for {@link RunCommand}
 */
public class RunConstants {
   /**
    * Default number of threads
    */
   public static final int DEFAULT_NUMBER_OF_THREADS = 4;
   /**
    * Default size of cache. Bound means that each thread will try to generate
    * instances with seeds from 0 - 99. It follows that cache will be able to have
    * only 100 instances.
    */
   public static final int DEFAULT_BOUND_OF_SEEDS = 100;
   /**
    * Default lifetime of thread. If it equals 100 that means that each thread will
    * try to call the cache 100 times. After that it ends the process.
    */
   public static final int DEFAULT_LIFETIME_IF_THREAD = 200;
   /**
    * Default memory limit for the cache.
    */
   public static final int DEFAULT_MEMORY_LIMIT = 100;
}
