package aleksy.pwr.javaa.knapsackthreads.constants;

import aleksy.abspec.commandaplicationengine.constants.Prefixes;

/**
 * Application information
 */
public final class Application {
   /**
    * Application name
    */
   public static final String NAME = "Knapsack Threads";
   /**
    * Application version
    */
   public static final String VERSION = "0.2.0";
   /**
    * Author of application
    */
   public static final String AUTHOR = "Aleksy Bernat 226023";
   /**
    * Application command short name
    */
   public static final String COMMAND_NAME = "ktr";
   /**
    * Prefix for subcommands
    */
   public static final String SUBCOMMAND_PREFIX = Prefixes.DEFAULT_SUBCOMMAND_PREFIX;
   /**
    * Prefix for flags
    */
   public static final String FLAG_PREFIX = Prefixes.DEFAULT_FLAG_PREFIX;
   /**
    * Prefix for arguments
    */
   public static final String ARG_PREFIX = Prefixes.DEFAULT_ARGUMENT_PREFIX;
   /**
    * Package with classes annotated by {@link aleksy.abspec.commandaplicationengine.annotation.Command}
    */
   public static final String PACKAGE = "aleksy";
   /**
    * Additional information of application
    */
   public static final String INFORMATION = "Application created for Java Advanced classes";
   /**
    * Main directory with algorithms
    */
   public static final String MAIN_DIRECTORY_WITH_ALGORITHMS = "algorithms/";
   /**
    * Path to directory with algorithm classes (with package hierarchy)
    */
   public static final String ALGORITHMS_DIRECTORY = MAIN_DIRECTORY_WITH_ALGORITHMS + "aleksy/pwr/javaa/knapsacklib/logic/impl/";
   /**
    * Package directory to algorithms
    */
   public static final String ALGORITHMS_PACKAGE = "aleksy.pwr.javaa.knapsacklib.logic.impl.";
}
