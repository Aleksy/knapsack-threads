package aleksy.pwr.javaa.knapsackthreads.enumerate;

/**
 * Cache type
 */
public enum CacheType {
   /**
    * Weak reference (weak hash map)
    */
   WEAK_REFERENCE,
   /**
    * Strong reference (hash map)
    */
   STRONG_REFERENCE,
   /**
    * Phantom reference (hash map)
    */
   PHANTOM_REFERENCE
}
