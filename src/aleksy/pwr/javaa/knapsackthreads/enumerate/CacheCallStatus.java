package aleksy.pwr.javaa.knapsackthreads.enumerate;

/**
 * Cache call status
 */
public enum CacheCallStatus {
   /**
    * When new instance was added
    */
   MISS,
   /**
    * When in the cache was an instance with the same seed
    */
   HIT
}
