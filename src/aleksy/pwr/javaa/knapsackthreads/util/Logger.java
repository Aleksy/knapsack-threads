package aleksy.pwr.javaa.knapsackthreads.util;

import java.time.Instant;

/**
 * Logger
 */
public class Logger {
   /**
    * Logs a message for thread with given name
    *
    * @param name    of thread
    * @param message to print
    */
   public synchronized static void log(String name, String message) {
      System.out.println(Instant.now() + "   " + name + " ::   " + message);
   }

   /**
    * Logs a message for default MAIN thread
    *
    * @param message to print
    */
   public synchronized static void log(String message) {
      System.out.println(Instant.now() + "   MAIN     ::   " + message);
   }
}
