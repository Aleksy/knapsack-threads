package aleksy.pwr.javaa.knapsackthreads.util;

import aleksy.pwr.javaa.knapsacklib.logic.api.Algorithm;
import aleksy.pwr.javaa.knapsackthreads.constants.Application;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RunCommandUtil {
   public static void loadAlgorithms(List<Algorithm> algorithms) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
      String path = Application.MAIN_DIRECTORY_WITH_ALGORITHMS;
      File directory = new File(path);
      List<File> files = createFilesList();
      ClassLoader loader = createLoader(directory);
      List<File> filteredFiles = filterOnlyClassFiles(files);
      createClassesList(filteredFiles, loader, algorithms);
   }


   private static List<Algorithm> createClassesList(List<File> filteredFiles, ClassLoader loader, List<Algorithm> algorithms) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
      for (File file : filteredFiles) {
         Class<?> clazz = loader.loadClass(Application.ALGORITHMS_PACKAGE + file.getName()
            .replaceAll(".class", ""));
         algorithms.add((Algorithm) clazz.newInstance());
      }
      return algorithms;
   }

   private static List<File> filterOnlyClassFiles(List<File> files) {
      return files.stream().filter(file -> file.getName().endsWith(".class")).collect(Collectors.toList());
   }

   private static List<File> createFilesList() {
      return new ArrayList<>(Arrays.asList(Objects.requireNonNull(
         new File(Application.ALGORITHMS_DIRECTORY).listFiles())));
   }

   private static ClassLoader createLoader(File directory) throws MalformedURLException {
      URL url = directory.toURI().toURL();
      URL[] urls = {url};
      return new URLClassLoader(urls);
   }
}
