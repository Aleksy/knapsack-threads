## Knapsack Problem Threads Application

Knapsack Threads is an application written for Java Advanced classes.

### Running in IDE

Application works with two libraries:
1. [Javaa Knapsack Library](https://gitlab.com/Aleksy/javaa-knapsack-library)
2. [Command Application Engine](https://gitlab.com/Aleksy/command-application-engine)

If you would to run the KT in your IDE you need to download JARs of this libraries.   
   
Javaa Knapsack Library is my private project, you can find the JAR [here](https://drive.google.com/open?id=1-6QZfpRTxaFGeXCmcIi78sgjVi1xtr9i).   
Now you can attach the libraries to KT project in your IDE.

### Testing

Command Application Engine requires path as a first argument for the program. So if you would like to test the KT you need
to give following arguments to the program:
```
<path> <command_name>
```

The KT application is not using the path so you can type for instance
```
C:
```

Give help as command name to test if everything works properly. You should see log like this:
```
====== COMMANDS ======

run - starts the application
info - displays the information about the application
help - displays names and descriptions of the commands

======== INFO ========

To call the command with the arguments type:
ktr command_name -arg0 val0 -arg1 val1
To add the flag to the command type:
ktr command_name /flag0 /flag1
To call the command with its subcommand type:
ktr command_name :subcommand


Simple example:
calculator divide :of-integers /display-with-modulo -x 23 -y 17


To display detail help description for the command type:
ktr command_name /h

```

Now you can type other commands and test the application.

### Installation

To install the KT you need to create a JAR from KT. After that put it in path which is in environment variable PATH.   
Now create a bash file and type there:
```
@echo off
java -jar "C:\<DIRECTORY_WHERE_IS_KT_APP_JAR>\knapsack-threads.jar" %CD% %*
```

File should have name
```
ktr.bat
```

Now you can run command prompt and try tu run the application:
```
ktr help
```